# test-project

## Installation

* composer install
* yarn install --freeze-lock
* php bin/console doctrine:database:drop --if-exists
* php bin/console doctrine:database:create
* php bin/console doctrine:migration:migrate

## How to run

* symfony serve --no-tls

In a second console:

* yarn watch