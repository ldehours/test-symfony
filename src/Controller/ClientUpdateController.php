<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\Type\CreateClientType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ClientUpdateController extends AbstractController
{
    /**
     * @Route("/client/update/{id}", name="client_update")
     */
    public function index(Request $request , $id , validatorInterface $validator, Session $session)
    {

        $clienRepo = $this->getDoctrine()->getRepository(Client::class);
        // if client exist (id exist), return bool

        //if data about user if find else return null
        $client = $clienRepo->find($id);

        //create new form client
        $form_client = new Client();

        // set to form data about user
        $form_client->setNom($client->getNom());
        $form_client->setPrenom($client->getPrenom());
        $form_client->setDateNaissance($client->getDateNaissance());
        $form_client->setTel($client->getTel());
        $form_client->setMail($client->getMail());
        $form_client->setAddrDomicile($client->getAddrDomicile());
        $form_client->setRpps($client->getRpps());

        $form = $this->createForm(CreateClientType::class, $form_client);


        $form->handleRequest($request);

        $errors = $validator->validate($form_client);

        // verifie le length de tel et de rpps
        $errors_length = [];

        if($form->isSubmitted())
        {
            $datas = $form->getData();



              $nom = $datas->getNom();
              $prenom = $datas->getPrenom();
              $date_naissance = $datas->getDateNaissance();
              $mail = $datas->getMail();
              $tel = $datas->getTel();
              $addr_domicile = $datas->getAddrDomicile();
              $rpps = $datas->getRpps();

            if(strlen($tel) != "10"){
                array_push($errors_length, [
                    'tel' => "tel",
                    'message' => 'Le numéro est incorrecte'
                ]);
            }

            if(strlen($rpps) != "11"){
                array_push($errors_length, [
                    'rpps' => "rpps",
                    'message' => 'Le rpps est incorrecte'
                ]);
            }

            if(count($errors_length) <= 0){

                if($form->isValid()){

                    $em = $this->getDoctrine()->getManager();

                    $client->setNom($nom);

                    $em->flush();

                    $this->addFlash('success','Update du client réussi.');

                    return $this->redirectToRoute('client_update',['id' => $id]);


                }

            }


        }

        return $this->render('client_update/index.html.twig', [
            'controller_name' => 'ClientUpdateController',
            'client' => $client,
            'form' => $form->createView()
        ]);
    }
}
