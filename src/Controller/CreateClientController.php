<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\Type\CreateClientType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateClientController extends AbstractController
{
    /**
     * @Route("/create/client", name="create_client")
     */
    public function index(Request $request, validatorInterface $validator)
    {
        $client = new Client();

        $form = $this->createForm(CreateClientType::class, $client);


        $form->handleRequest($request);

        $errors = $validator->validate($client);

        // verifie le length de tel et de rpps
        $errors_length = [];

        if($form->isSubmitted())
        {
            $datas = $form->getData();


//            $nom = $datas->getNom();
//            $prenom = $datas->getPrenom();
//            $date_naissance = $datas->getDateNaissance();
//            $mail = $datas->getMail();
              $tel = $datas->getTel();
//            $addr_domicile = $datas->getAddrDomicile();
              $rpps = $datas->getRpps();

            if(strlen($tel) != "10"){
                array_push($errors_length, [
                    'tel' => "tel",
                    'message' => 'Le numéro est incorrecte'
                ]);
            }

            if(strlen($rpps) != "11"){
                array_push($errors_length, [
                    'rpps' => "rpps",
                    'message' => 'Le rpps est incorrecte'
                ]);
            }

            if(count($errors_length) <= 0){

                if($form->isValid()){

                    $em = $this->getDoctrine()->getManager();

                    $em->persist($datas);
                    $em->flush();

                    $this->addFlash('success','Création du client réussi.');
                    return $this->redirectToRoute('create_client');



                }

            }


        }




        return $this->render('create_client/index.html.twig', [
            'form' => $form->createView(),
            'error_length' => $errors_length
        ]);
    }
}
