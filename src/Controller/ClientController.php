<?php

namespace App\Controller;

use App\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ClientController extends AbstractController
{
    /**
     * @Route("/client", name="client")
     */
    public function index()
    {

        $em = $this->getDoctrine()->getRepository(Client::class);

        // all clients
        $client = $em->findAll();



        return $this->render('client/index.html.twig', [
            'controller_name' => 'ClientController',
            'clients' => $client
        ]);
    }
}
