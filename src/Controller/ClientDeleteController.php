<?php

namespace App\Controller;

use App\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ClientDeleteController extends AbstractController
{
    /**
     * @Route("/client/delete/{client}", name="client_delete", defaults={"client" : null} )
     *
     */
    public function index(Request $request, Client $client)
    {

        //if data about user if find else return null

        if($client){


            $clienRepo = $this->getDoctrine()->getManager()->remove($client);
            $em = $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('client');


        }else{

            return $this->redirectToRoute('client');

        }

    }
}
