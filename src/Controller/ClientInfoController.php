<?php

namespace App\Controller;

use App\Entity\Client;
use http\Env\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ClientInfoController extends AbstractController
{
    /**
     * @Route("/client/info/{id}", name="client_info")
     */
    public function index(\Symfony\Component\HttpFoundation\Request $request , $id)
    {

        $em = $this->getDoctrine()->getRepository(Client::class);
        // if client exist (id exist), return bool
        //if data about user if find else return null
        $client = $em->find($id);



        return $this->render('client_info/index.html.twig', [
            'controller_name' => 'ClientInfoController',
            'client' => $client
        ]);
    }
}
