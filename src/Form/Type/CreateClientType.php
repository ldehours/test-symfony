<?php

namespace App\Form\Type;
use App\Entity\Client;


use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateClientType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class,[
                "attr" => [
                    "placeholder" => "Entrer un nom"
                ]
            ])
            ->add('prenom', TextType::class,[
                "attr" => [
                    "placeholder" => "Entrer un prénom"
                ]
            ])
            ->add('date_naissance', BirthdayType::class,[

            ])

            ->add('mail', EmailType::class,[
                "attr" => [
                    "placeholder" => "Entrer un mail"
                ]
            ])
            ->add('tel', TextType::class,[
                "attr" => [
                    "placeholder" => "Entrer un numéro de téléphone"
                ]
            ])
            ->add('addr_domicile', TextType::class,[
                "attr" => [
                    "placeholder" => "Entrer une adresse de domicile"
                ]
            ])
            ->add('rpps', IntegerType::class,[
                "attr" => [
                    "placeholder" => "Entrer un numéro rpps"
                ]
            ])
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
        ]);
    }

}