<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ est vide")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Votre champ doit contenir des lettres"
     * )
     *
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ est vide")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Votre champ doit contenir des lettres"
     * )
     */
    private $prenom;

    /**
     * @ORM\Column(type="date")
     */
    private $date_naissance;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ est vide")
     * @Assert\Email(
     *     message = "L'émail '{{ value }}' n'est pas valide."
     * )
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank(message="Le champ est vide")
     * @Assert\Regex(
     *     pattern="/^[0-9]$/",
     *     match=false,
     *     message="Votre numéro est incorrect"
     * )
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ est vide")
     * @Assert\Regex(
     *     pattern="/^([a-z0-9])+$/",
     *     match=false,
     *     message="Votre adresse de domicile est incorrecte"
     * )
     */
    private $addr_domicile;

    /**
     * @ORM\Column(type="string", length=11)
     * @Assert\NotBlank(message="Le champ est vide")
     * @Assert\Regex(
     *     pattern="/^[0-9]$/",
     *     match=false,
     *     message="Rpps invalide"
     * )
     */
    private $rpps;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->date_naissance;
    }

    public function setDateNaissance(\DateTimeInterface $date_naissance): self
    {
        $this->date_naissance = $date_naissance;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getAddrDomicile(): ?string
    {
        return $this->addr_domicile;
    }

    public function setAddrDomicile(string $addr_domicile): self
    {
        $this->addr_domicile = $addr_domicile;

        return $this;
    }

    public function getRpps(): ?string
    {
        return $this->rpps;
    }

    public function setRpps(string $rpps): self
    {
        $this->rpps = $rpps;

        return $this;
    }
}
